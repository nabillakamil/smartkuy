<!DOCTYPE html>
<html lang="en">
    <?php include 'headerc.php'; ?>   
        <div class="section secondary-section" style="border-top: 2px solid white; border-bottom: 2px solid white;">
                <div class="container">
                    <h1 style="text-align: left">QUIZ 1</h1>
                    <div class="row-fluid">
                        <div class="span6" style="border-right: 3px solid white" >
                            <div>
                                <h4 style="color: black">1) Tampilkanlah seluruh field yang berada didalam tabel barang</h4>
                                <label for="question-1-answers-A">
                                    <input type="radio" name="question-1-answers" id="question-1-answers-A" value="A" />
                                    <span class="checkmark"></span>A. select barang;
                                </label>
                                <label for="question-1-answers-B">
                                    <input type="radio" name="question-1-answers" id="question-1-answers-A" value="B" />
                                    <span class="checkmark"></span>B. select * from barang;
                                </label>
                                <label for="question-1-answers-C">
                                    <input type="radio" name="question-1-answers" id="question-1-answers-A" value="C" />
                                    <span class="checkmark"></span>C. select * barang;
                                </label>
                                <label for="question-1-answers-D">
                                    <input type="radio" name="question-1-answers" id="question-1-answers-A" value="D" />
                                    <span class="checkmark"></span>D. select * barang
                                </label>
                            </div>
                            <div>
                                <h4 style="color: black">2) Cara menghapus tabel barang</h4>
                                <label for="question-1-answers-A">
                                    <input type="radio" name="question-2-answers" id="question-2-answers-A" value="A" />
                                    <span class="checkmark"></span>A. drop table * barang
                                </label>
                                <label for="question-1-answers-B">
                                    <input type="radio" name="question-2-answers" id="question-2-answers-A" value="B" />
                                    <span class="checkmark"></span>B. drop table barang
                                </label>
                                <label for="question-1-answers-C">
                                    <input type="radio" name="question-2-answers" id="question-2-answers-A" value="C" />
                                    <span class="checkmark"></span>C. drop table barang;
                                </label>
                                <label for="question-1-answers-D">
                                    <input type="radio" name="question-2-answers" id="question-2-answers-A" value="D" />
                                    <span class="checkmark"></span>D. drop tble_barang;
                                </label>
                            </div>
                            <div>
                                <h4 style="color: black">3) Hapus hak akses dari user smartkuy</h4>
                                <label for="question-1-answers-A">
                                    <input type="radio" name="question-3-answers" id="question-3-answers-A" value="A" />
                                    <span class="checkmark"></span>A. Revoke delete on karyawan from smartkuy;
                                </label>
                                <label for="question-1-answers-B">
                                    <input type="radio" name="question-3-answers" id="question-3-answers-A" value="B" />
                                    <span class="checkmark"></span>B. Revoke delete on karyawan smartkuy;
                                </label>
                                <label for="question-1-answers-C">
                                    <input type="radio" name="question-3-answers" id="question-3-answers-A" value="C" />
                                    <span class="checkmark"></span>C. Revoke delete karyawan from smartkuy;
                                </label>
                                <label for="question-1-answers-D">
                                    <input type="radio" name="question-3-answers" id="question-3-answers-A" value="D" />
                                    <span class="checkmark"></span>D. Revoke delete on karyawan from smartkuy
                                </label>
                            </div>
                        </div>
                        <div class="span6" >
                            <div>
                                <h4 style="color: black">4) Data yang terletak didalam satu kolom disebut</h4>
                                <label for="question-1-answers-A">
                                    <input type="radio" name="question-4-answers" id="question-4-answers-A" value="A" />
                                    <span class="checkmark"></span>A. Field
                                </label>
                                <label for="question-1-answers-B">
                                    <input type="radio" name="question-4-answers" id="question-4-answers-A" value="B" />
                                    <span class="checkmark"></span>B. Record
                                </label>
                                <label for="question-1-answers-C">
                                    <input type="radio" name="question-4-answers" id="question-4-answers-A" value="C" />
                                    <span class="checkmark"></span>C. DBMS
                                </label>
                                <label for="question-1-answers-D">
                                    <input type="radio" name="question-4-answers" id="question-4-answers-A" value="D" />
                                    <span class="checkmark"></span>D. Tabel
                                </label>
                            </div>
                            <div>
                                <h4 style="color: black">5) soal Data yang terletak satu baris dan mewakili satu objek disebut</h4>
                                <label for="question-1-answers-A">
                                    <input type="radio" name="question-5-answers" id="question-5-answers-A" value="A" />
                                    <span class="checkmark"></span>A. Field
                                </label>
                                <label for="question-1-answers-B">
                                    <input type="radio" name="question-5-answers" id="question-5-answers-A" value="B" />
                                    <span class="checkmark"></span>B. Record
                                </label>
                                <label for="question-1-answers-C">
                                    <input type="radio" name="question-5-answers" id="question-5-answers-A" value="C" />
                                    <span class="checkmark"></span>C. DBMS
                                </label>
                                <label for="question-1-answers-D">
                                    <input type="radio" name="question-5-answers" id="question-5-answers-A" value="D" />
                                    <span class="checkmark"></span>D. Tabel
                                </label>
                            </div>
                            <div>
                                <h4 style="color: black">6) Data yang menggambarkan kumpulan karakteristik suatu entitas</h4>
                                <label for="question-1-answers-A">
                                    <input type="radio" name="question-6-answers" id="question-6-answers-A" value="A" />
                                    <span class="checkmark"></span>A. Field
                                </label>
                                <label for="question-1-answers-B">
                                    <input type="radio" name="question-6-answers" id="question-6-answers-A" value="B" />
                                    <span class="checkmark"></span>B. Record
                                </label>
                                <label for="question-1-answers-C">
                                    <input type="radio" name="question-6-answers" id="question-6-answers-A" value="C" />
                                    <span class="checkmark"></span>C. DBMS
                                </label>
                                <label for="question-1-answers-D">
                                    <input type="radio" name="question-6-answers" id="question-6-answers-A" value="D" />
                                    <span class="checkmark"></span>D. Database
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="container" style="text-align: right;">
                        <a href="quiz.php" ><button class="btn btn-info" onclick="myFunction()" style="width: 100px;">finish</button></a>
                    </div>
                </div>            
            </div>

    <?php include 'footer.php' ?>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript">
            function myFunction() {
               alert("SCORE : 50");
            }
        </script>
    </body>
</html>

