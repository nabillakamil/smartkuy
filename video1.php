<!doctype>
<?php include "config.php" ?>
<?php include "headerc.php" ?>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="section primary-section">
		<div class="container" style="border-top: 2px solid white;">
			<div class="row-fluid">
		        <a href="menu_video.php">
		        	<img style="width: 35px; height: 30px;" src="images/b_back.png">
		        </a>
		    </div>
		    <div class="row-fluid team" id="first-person">
				<div align="center">
					<!-- <video width="800" height="480" controls>
					  <source src="tutorial membuat DDL & DML.mp4" type="video/mp4">
					  <source src="tutorial membuat DDL & DML.ogg" type="video/ogg">
					  Your browser does not support the video tag.
					</video> -->
					<iframe width="800" height="480" src="https://youtube.com/embed/1c5BhaepIiw"></iframe>
				</div>
				
			</div>
			<div class="row-fluid" align="center">
				<h1> Dasar DDL dan DML pada MySQL Database</h1>
				<p>Data Definition Language (DDL) merupakan teknik DBMS pembuatan objek basisdata, salah satunya adalah tabel.  DDL memiliki 3 perintah yaitu CREATE, ALTER dan DROP.
Data Manipulation Language (DML) merupakan teknik DBMS untuk melakukan pengelolaan data yang ada pada tabel di basisdata. DML memiliki 4 perintah utama yaitu INSERT, UPDATE, DELETE dan SELECT.</p>
				
			</div>
			<div align="center" class="row-fluid" style="margin-top: 20px; margin-bottom: 20px;">
				
			</div>
			<div class="row-fluid">
				<div align="center">
					<a href="javascript:void(0);" onclick="javascipt:window.open('tutorial membuat DDL & DML.mp4');" class="popup">
					 	<button type="button" class="btn btn-info btn-lg" style="width: 150px; height: 40px;">Unduh</button>
					 </a>
				 </div>
				
			</div>

	    </div>
	</div>
</body>

<?php include "footer.php" ?>