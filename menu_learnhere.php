<!DOCTYPE html>
<html>
<?php include 'config.php' ?>
<?php include 'headerc.php' ?>

<body>
	<div class="section primary-section" id="about">
            <div class="container" style="border-top: 2px solid white;">
            	<div class="row-fluid">
        			<a href="home.php">
        				<img style="width: 35px; height: 30px;" src="images/b_back.png">
        			</a>
        		</div>
                <div class="title">
                    <h1>Ruang Belajar</h1>
                </div>
                <div class="row-fluid team">
                    <div class="span4" id="first-person">
                        <div class="thumbnail">
                            <img style="width: 250px; height: 250px;" src="images/book1.png" alt="team 1">
                            <br>
                            <h3>Buku Elektronik</h3>
                            <div class="mask">
                                <h2>Buku Elektronik</h2>
                            <p>Kumpulan elektronik buku yang berisikan materi - materi DDL & DML <br> Ayo belajar!</p>
                            <a href="menu_books.php">
                            <button style="background-color: black; color: white; width: 130px;height: 60px; font-size: 18px; margin-top: 30px;" >Belajar</button>
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="span4" id="second-person">
                            <div class="thumbnail">
                                <img style="width: 250px; height: 250px;" src="images/video1.png" alt="team 1">
                                <br>
                                <h3>Video</h3>
                                <div class="mask">
                                    <h2>Video</h2>
                                    <p>Kumpulan Video yang berisikan materi-materi DDL & DML <br> Ayo belajar!</p>
                                    <a href="menu_video.php">
                                    <button style="background-color: black; color: white; width: 130px;height: 60px; font-size: 18px; margin-top: 30px;" >Belajar</button></a>
                                </div>
                            </div>
                        </div>
                    <div class="span4" id="third-person">
                        <div class="thumbnail">
                            <img style="width: 250px; height: 250px;" src="images/audio1.png" alt="team 1">
                            <br>
                            <h3>Suara</h3>
                            <div class="mask">
                                <h2>Suara</h2>
                                <p>Kumpulan Audio yang berisikan Materi-materi DDL & DML  <br> Ayo belajar!</p>
                                <a href="menu_audio.php">
                                <button style="background-color: black; color: white; width: 130px;height: 60px; font-size: 18px; margin-top: 30px;" >Belajar</button> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
</body>
<?php include 'footer.php' ?>
</html>