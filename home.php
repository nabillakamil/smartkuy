<!DOCTYPE html>
<!--
 * A Design by GraphBerry
 * Author: GraphBerry
 * Author URL: http://graphberry.com
 * License: http://graphberry.com/pages/license
-->
<?php include "config.php"; ?>
<?php include 'header.php'; ?>
<html lang="en">    
        <!-- Start home section --x>
        <div id="home">
            <!-- Start cSlider -->
            <div id="da-slider" class="da-slider">
                <div class="triangle"></div>
                <!-- mask elemet use for masking background image -->
                <div class="mask"></div>
                <!-- All slides centred in container element -->
                <div class="container">
                    <!-- Start first slide -->
                    <div class="da-slide">
                        
                        <h2 class="fittext2">SMART KUY!</h2>
                        <h4>Belajar jadi lebih mudah dan seru!</h4>
                        <p><br> Smart Kuy! merupakan website yang memberikan pelayanan berupa materi-materi pembelajaran megenai DDL dan DML</p>
                        <div class="da-img">
                            <img src="images/Slider1.png" alt="image01" width="300">
                        </div>
                    </div>
                    <!-- End first slide -->
                    <!-- Start second slide -->
                    <div class="da-slide">
                        <h2>Memberi Kemudahan</h2>
                        <h4>Mudah Digunakan!</h4>
                        <p> <br> Metode pembelajaran bersama Smart Kuy! memberi kemudahan dalam mendapatkan materi pembelajaran. Sehingga belajar terasa lebih mudah.</p>
                        <div class="da-img">
                            <img src="images/Slider2.png" width="300" alt="image02">
                        </div>
                    </div>
                    <!-- End second slide -->
                    <!-- Start third slide -->
                    <div class="da-slide">
                        <h2>Memberi Kenyamanan</h2>
                        <h4>Bikin Ketagihan!</h4>
                        <p><br> Kenyamanan dalam mengakses berbagai informasi yang ada di Smart Kuy! memberikan rasa nyaman yang bikin ketagihan. </p>
                        <div class="da-img">
                            <img src="images/Slider3.png" width="300" alt="image03">
                        </div>
                    </div>
                    <!-- Start third slide -->
                    <!-- Start cSlide navigation arrows -->
                    <div class="da-arrows">
                        <span class="da-arrows-prev"></span>
                        <span class="da-arrows-next"></span>
                    </div>
                    <!-- End cSlide navigation arrows -->
                </div>
            </div>
        </div>
        <!-- End home section -->
        <!-- Service section start -->
        <div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Apa yang SmartKuy Berikan?</h1>
                    <br>
                    <!-- Section's title goes here -->
                    <p>Smart Kuy! Memberikan banyak kemudahan. </p>
                    <br>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <a href="menu_learnhere.php"><img class="img-circle" src="images/Service1.png" alt="service 1"></a>
                            </div>
                            <h3>Ruang Belajar</h3>
                            <p>Temukan materi pembelajaranmu di sini!</p>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <a href="quiz.php"><img class="img-circle" src="images/Service3.png" alt="service 3"></a>
                            </div>
                            <h3>Kuis</h3>
                            <p>Ayo uji kemampuan kamu!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Service section end -->
        <!-- Portfolio section start -->

        <div class="section primary-section" id="about">
            <div class="triangle"></div>
            <div class="container">
                <div class="title">
                    <h1>Siapa Kami?</h1>
                </div>
                <div class="row-fluid team">
                    <div class="span3" id="first-person">
                        <div class="thumbnail">
                            <img src="images/Tim1.jpg" alt="team 1">
                            <h3>Dian Chusnul H</h3>
                            <ul class="social">
                                <li>
                                    <a href="">
                                        <span class="icon-facebook-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="icon-twitter-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="icon-linkedin-circled"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="mask">
                                <h2>Master Web</h2>
                            <p>Bicara itu gampang. <br> Tunjukkan kodingannya!</p>
                            </div>
                        </div>
                    </div>
                    <div class="span3" id="second-person">
                            <div class="thumbnail">
                                <img src="images/Tim2.jpg" alt="team 1">

                                <h3>Nabilla Kamil</h3>
                                <ul class="social">
                                    <li>
                                        <a href="">
                                            <span class="icon-facebook-circled"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span class="icon-twitter-circled"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span class="icon-linkedin-circled"></span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="mask">
                                    <h2>Pemrogram Web</h2>
                                    <p>Pertama, selesaikan masalahnya. <br> Kemudian, tulis kodingannya!</p>
                                </div>
                            </div>
                        </div>
                    <div class="span3" id="third-person">
                        <div class="thumbnail">
                            <img src="images/Tim3.jpg" alt="team 1">
                            <h3>RR Ayuni</h3>
                            <ul class="social">
                                <li>
                                    <a href="">
                                        <span class="icon-facebook-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="icon-twitter-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="icon-linkedin-circled"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="mask">
                                <h2>Pemrogram Web</h2>
                                <p>Jika kamu mampu menulis, "halo dunia", <br> kamu dapat merubah dunia</p>
                            </div>
                        </div>
                    </div>
                    <div class="span3" id="fourth-person">
                        <div class="thumbnail">
                            <img src="images/Tim4.jpg" alt="team 1">
                            <h3>Mela Mai A</h3>
                            <ul class="social">
                                <li>
                                    <a href="">
                                        <span class="icon-facebook-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="icon-twitter-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="icon-linkedin-circled"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="mask">
                                <h2>Pendesain Web</h2>
                                <p>Jika kamu pikir matemarika sulit, <br> coba desain web!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-text centered">
                    <h4>Tentang Kami</h4>
                    <p>Smart Kuy! merupakan hasil karya dari mahasiswa S1 Teknik Informatika yang berasal dari kelas IF 40-09. Smart Kuy! merupakan produk nyata dari hasil pembelajaran pada Mata Kuliah Interaksi Manusia dan Komputer.</p>
                </div>

            </div>
        </div>
        <!-- About us section end -->
        <!-- Contact section start -->
        <div id="contact" class="contact">
            <div class="section secondary-section">
                <div class="container">
                    <div class="title">
                        <h1>Hubungi</h1>
                        <p>Hubungi kami di :</p>
                    </div>
                </div>
                <div class="container">
                    <div class="span9 center contact-info">
                        <p>Telkom University, Bandung - Indonesia</p>
                        <p class="info-mail">SmartKuy@gmail.com</p>
                        <p>081212345678</p>
                        <div class="title">
                            <h3>Media Sosial Kami</h3>
                        </div>
                    </div>
                    <div class="row-fluid centered">
                        <ul class="social">
                            <li>
                                <a href="">
                                    <span class="icon-facebook-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/TheSmartKuy">
                                    <span class="icon-twitter-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-linkedin-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-pinterest-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-dribbble-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-gplus-circled"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact section edn -->
        <!-- Footer section start -->
        <?php include 'footer.php' ?>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>