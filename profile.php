

<!DOCTYPE html>
<!--
 * A Design by GraphBerry
 * Author: GraphBerry
 * Author URL: http://graphberry.com
 * License: http://graphberry.com/pages/license
-->
<html lang="en">
    <?php include 'headerc.php'; ?> 
            <div class="section secondary-section">
                <div class="container">
                    <div class="row-fluid">
                        <div class="span3" style="text-align: center; ">
                            <div>
                                <img src="images/tim1.jpg" style="width: 50%; height: 50%; border-radius: 50%; margin-top: 30px ">
                                <p style="color: black">Dian Chusnul Hidayati</p>
                            </div>
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Feedback</button>
                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content " style="background-color: black;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Feedback</h4>
                              </div>
                              <div class="modal-body">
                                <form align="center" action="home.php">
                                    <div class="form-group">
                                    <label>Nama:</label>
                                    <input class="form-control" type="text" name="name" >
                                    <label  >E-mail:</label> </br>
                                    <input type="email" class="form-control" name="email">
                                    <label>feedback:</label></br>
                                    <textarea name="message" class="form-control" rows="10" cols="30">
                                        
                                    </textarea></br>
                                  </div>
                                  
                                </form>
                              </div>
                              <div class="modal-footer" style="background-color: black">
                                <button type="button" class="btn btn-default" style="width: 80px; height: 40px;" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-info " style="width: 80px; height: 40px;" onclick="myFunction1()" > Ajukan</button>
                              </div>
                            </div>

                          </div>
                        </div>
                        </div>
                        <div class="span1"></div>
                        <div class="span7" style="text-align: center;border-right: 3px solid white;border-top: 3px solid white;border-left: 3px solid white; border-bottom: 3px solid white">
                            <h3 style="color: black; text-align: center">PROFIL</h3>
                            <form class="form-hordizontal" action="profile.php">
                                <div class="form-group">
                                    <div class="col-sm-10" style="margin-bottom: 3px">
                                        <input type="text" class="form-control" id="name" placeholder="nama" name="name" style="width: 60%">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10" style="margin-bottom: 3px">
                                        <input type="email" class="form-control" id="phone" placeholder="number phone" name="phone" style="width: 60%">
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 3px">
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="address" placeholder="address" name="address" style="width: 60%">
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 3px">
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" style="width: 60%">
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 3px">
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="username" placeholder="username" name="username" style="width: 60%">
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 3px"s>
                                    <div class="col-sm-10">          
                                        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" style="width: 60%">
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 20px; float: right; margin-right: 125px">        
                                  <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-info" onclick="myFunction()">Ubah Profil</button>
                                  </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
        </div> 
        <div class="section secondary-section">
            
        </div>
        
        <!-- Contact section edn -->
        <!-- Footer section start -->
        <?php include 'footer.php'; ?> 
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript">
            function myFunction() {
               alert("Data berhasil diubah !");
            }
        </script>
         <script type="text/javascript">
            function myFunction1() {
               alert("Feedback berhasil dikirim ! ");
            }
        </script>
    </body>
</html>