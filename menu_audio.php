<!DOCTYPE>
<?php include"config.php" ?>

<?php include "headerc.php" ?>

<body>
	<div class="section primary-section" id="service">
        <div class="container" style="border-top: 2px solid white;">
        	<div class="row-fluid">
        		<a href="menu_learnhere.php">
        			<img style="width: 35px; height: 30px;" src="images/b_back.png">
        		</a>
        	</div>
          	<div class="row-fluid">
                <div class="span3">
                    <a href="audio1.php">
                    <div class="centered service">
                        <div class="circle-border zoom-in">
                            <img class="img-circle" src="images/audio1.png" alt="service 1">
                        </div>
                        <h3>Pengenalan</h3>
                        <p>Pengenalan DDL DML</p>
                    </div>
                    </a>
                </div>
            	<div class="span3">
                	<div class="centered service">
                    	<div class="circle-border zoom-in">
                        	<img class="img-circle" src="images/audio1.png" alt="service 2" />
                    	</div>
                    	<h3>Materi 1</h3>
                    	<p>Perancangan Basis Data</p>
                	</div>
             	</div>
             	<div class="span3">
                	<div class="centered service">
                    	<div class="circle-border zoom-in">
                        	<img class="img-circle" src="images/audio1.png" alt="service 3">
                    	</div>
                    	<h3>Materi 2 </h3>
                    	<p>DDL, DML, Operator & Functions</p>
                	</div>
            	</div>
            	<div class="span3">
                	<div class="centered service">
                    	<div class="circle-border zoom-in">
                        	<img class="img-circle" src="images/audio1.png" alt="service 3">
                    	</div>
                    	<h3>Materi 3</h3>
                    	<p>Union & Join</p>
                	</div>
            	</div>
            </div>
            <div class="row-fluid">
                <div class="span3">
                    <div class="centered service">
                        <div class="circle-border zoom-in">
                            <img class="img-circle" src="images/audio1.png" alt="service 1">
                        </div>
                        <h3>Materi 4</h3>
                        <p>Views</p>
                    </div>
                </div>
            	<div class="span3">
                	<div class="centered service">
                    	<div class="circle-border zoom-in">
                        	<img class="img-circle" src="images/audio1.png" alt="service 2" />
                    	</div>
                    	<h3>Materi 5</h3>
                    	<p>Store Procedure & Trigger</p>
                	</div>
             	</div>
             	<div class="span3">
                	<div class="centered service">
                    	<div class="circle-border zoom-in">
                        	<img class="img-circle" src="images/audio1.png" alt="service 3">
                    	</div>
                    	<h3>Materi 6</h3>
                    	<p>PHP MySQL</p>
                	</div>
            	</div>
            	<div class="span3">
                	<div class="centered service">
                    	<div class="circle-border zoom-in">
                        	<img class="img-circle" src="images/audio1.png" alt="service 3">
                    	</div>
                    	<h3>Materi 7</h3>
                    	<p>Materi Supplier</p>
                	</div>
            	</div>
            </div>
        </div>
    </div>
</body>

<?php include "footer.php" ?>