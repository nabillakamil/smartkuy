<!DOCTYPE html>
<!--
 * A Design by GraphBerry
 * Author: GraphBerry
 * Author URL: http://graphberry.com
 * License: http://graphberry.com/pages/license
-->
<html lang="en">
    <?php include 'headerc.php'; ?>    

        <div class="section primary-section" id="service">
            <div class="container" style="border-top: 2px solid white; border-bottom: 2px solid white;">
                <!-- Start title section -->
                <a href="home.php">
                    <img style="width: 35px; height: 30px;" src="images/b_back.png">
                </a>
                <div class="title">
                    <h1>K U I S</h1>
                    <!-- Section's title goes here -->
                    <p>Ayo Uji Kemampuan Kamu!</p>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <a href="isiquiz.php"><img class="img-circle" src="images/Service3.png" alt="service 1"></a>
                            </div>
                            <h3>Kuis 1</h3>
                            <p>Create Table</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service3.png" alt="service 2" />
                            </div>
                            <h3>Kuis 2</h3>
                            <p>Primary Key</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service3.png" alt="service 3">
                            </div>
                            <h3>Kuis 3</h3>
                            <p>Insert</p>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service3.png" alt="service 1">
                            </div>
                            <h3>Kuis 4</h3>
                            <p>Create Table</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service3.png" alt="service 2" />
                            </div>
                            <h3>Kuis 5</h3>
                            <p>Primary Key</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service3.png" alt="service 3">
                            </div>
                            <h3>Kuis 6</h3>
                            <p>Insert</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         
        <!-- Contact section edn -->
        <!-- Footer section start -->
       <?php include 'footer.php'; ?>     
</html>